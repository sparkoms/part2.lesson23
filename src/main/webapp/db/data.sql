CREATE TABLE person
(
    id         INT NOT NULL AUTO_INCREMENT,
    name       VARCHAR(250),
    birth_date VARCHAR(250),
    email      VARCHAR(50),
    phone      VARCHAR(15)
)